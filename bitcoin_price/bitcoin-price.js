const ccxt = require('ccxt')

const price = async (timeRange = '15m') => {
    const ohlcv = await new ccxt.okcoinusd().fetchOHLCV('BTC/USD', timeRange);
    const result = ohlcv.map((elem) => {
        let [date, open, high, low, close, value] = elem;
        return {
            date, open, high, low, close, value
        }
    });

    return result;

}
module.exports = { price };
